LCG_RELEASE=LCG_102b
LCG_ARCH=x86_64-centos7-gcc12
source /cvmfs/sft.cern.ch/lcg/views/${LCG_RELEASE}/${LCG_ARCH}-opt/setup.sh

if [[ "$HOSTNAME" == *"ithdp"* ]]; then
  # edge node
  echo "Sourcing hadoop edge node environment..."
  source hadoop-setconf.sh analytix 3.2 spark3
  echo "Done!"
elif [[ "$HOSTNAME" == *"lxplus"* ]]; then
  # lxplus
  echo "Sourcing lxplus environment..."
  source /cvmfs/sft.cern.ch/lcg/etc/hadoop-confext/hadoop-swan-setconf.sh analytix 3.2 spark3
  echo "Done!"
else
  echo "[Warning] Environment can only be lxplus or the CERN hadoop edge nodes. See README for more details"
  # still source lxplus env in case this is inside a condor node
  source /cvmfs/sft.cern.ch/lcg/etc/hadoop-confext/hadoop-swan-setconf.sh analytix
fi

# Compile the Roofit fitting function if it doesn't exist yet
if [ ! -f RooCMSShape_cc.so ]; then
    echo ""
    echo "Did not detect a RooCMSShape shared object file. Compiling with ACLiC... (should be needed only once)"
    root -l -b -q -e '.L RooCMSShape.cc+'
    echo "Done!"
fi

if [ ! -f RooErfExp_cc.so ]; then
    echo ""
    echo "Did not detect a RooErfExp shared object file. Compiling with ACLiC... (should be needed only once)"
    root -l -b -q -e '.L RooErfExp.cc+'
    echo "Done!"
fi
if [ ! -f RooCruijff_cxx.so ]; then
    echo ""
    echo "Did not detect a RooCruijff shared object file. Compiling with ACLiC... (should be needed only once)"
    root -l -b -q -e '.L RooCruijff.cxx+'
    echo "Done!"
fi
if [ ! -f RooDCBShape_cxx.so ]; then
    echo ""
    echo "Did not detect a RooDCBShape shared object file. Compiling with ACLiC... (should be needed only once)"
    root -l -b -q -e '.L RooDCBShape.cxx+'
    echo "Done!"
fi
